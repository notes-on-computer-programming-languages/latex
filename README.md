# latex

Document preparation system and markup language

## Unofficial documentation
* https://wiki.debian.org/Latex

### Epub output
* [latex to epub](https://google.com/search?q=latex+to+epub)
* [*Use LaTeX to produce Epub*](https://tex.stackexchange.com/questions/1551/use-latex-to-produce-epub)
  * latexml
  * latexmlpost
  * ebook-convert
* [*LaTeX document to epub or mobi ebook formats (with mathematical formulas) [duplicate]*
  ](https://tex.stackexchange.com/questions/16569/latex-document-to-epub-or-mobi-ebook-formats-with-mathematical-formulas)
  * tex4ht
* [(fr)](https://www.xm1math.net/doculatex/epub.html)
  * tex4ht

### inputenc
* [latex inputencoding latin1](https://google.com/search?q=latex+inputencoding+latin1)
* [*Why bother with inputenc and fontenc?*](https://texfaq.org/FAQ-why-inp-font)
* (fr) [*LaTeX et encodage*](https://www.xm1math.net/doculatex/encodage.html)